import React from 'react'
import { render } from 'react-dom'
import App from './app'
import '../stylus/main.styl'
import { createStore, applyMiddleware, compose } from 'redux'
import { Provider } from 'react-redux'
import todoReducer from './reducers/todos'
import thunk from 'redux-thunk'

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  todoReducer,
  composeEnhancer(applyMiddleware(thunk)),
)

render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('app'),
)
