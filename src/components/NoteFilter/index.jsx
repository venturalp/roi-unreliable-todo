import React, { Component } from 'react'
import PropTypes from 'prop-types'

class NoteFilter extends Component {

  state = {
    urgency: '',
    isCompleted: '',
  }

  handleChange = (e) => {
    const { name, value } = e.target
    const { updateFilter } = this.props

    let getValue = value

    if (name === 'isCompleted') {
      getValue = value === '' ? null : (value === 'true')
      this.setState({ isCompleted: value === '' ? value : (value === 'true') }, () => updateFilter(name, getValue))

      return
    }

    if (name === 'urgency') {
      getValue = value !== '' ? parseInt(value, 10) : null
    }

    this.setState({
      [name]: value,
    }, () => updateFilter(name, getValue))

  }

  render() {
    const { className } = this.props
    const { urgency, isCompleted } = this.state

    return (
      <form className={`note-filter ${className || ''}`}>
        <h2>Filter</h2>
        <div>
          <div className="input-group">
            <label>Urgency level: </label>
            <select value={urgency} onChange={this.handleChange} name="urgency">
              <option value="">Urgency level</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="input-group">
            <label>Completed? </label>
            <div>
              <input
                type="radio"
                name="isCompleted"
                checked={isCompleted === ''}
                value=""
                onChange={this.handleChange}
              />
              <label>All</label>
            </div>
            <div>
              <input
                type="radio"
                name="isCompleted"
                checked={isCompleted}
                value={true}
                onChange={this.handleChange}
              />
              <label>Completed</label>
            </div>
            <div>
              <input
                type="radio"
                name="isCompleted"
                checked={isCompleted === false}
                value={false}
                onChange={this.handleChange}
              />
              <label>Not completed</label>
            </div>
          </div>
        </div>
      </form>
    )
  }

}

NoteFilter.propTypes = {
  className: PropTypes.string,
  updateFilter: PropTypes.func,
}

export default NoteFilter
