import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { faSdCard, faTrashAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class Note extends Component {

  state = {
    msgError: '',
  }

  note = {}

  save = () => {
    const { saveNote, id } = this.props
    const { text, urgency, isCompleted } = this.note

    this.setState({ msgError: '' })

    if (!urgency.value) {
      this.setState({ msgError: 'Please select the note\'s urgency' })

      return
    }

    saveNote({
      text: text.value,
      urgency: parseInt(urgency.value, 10),
      isCompleted: isCompleted.checked,
      id,
    })
  }

  delete = () => {
    const { deleteNote, id } = this.props

    deleteNote(id)
  }

  render() {

    const { text, created, updated, isCompleted, urgency } = this.props
    const { msgError } = this.state

    return (
      <div className="note">
        <textarea defaultValue={text} ref={field => this.note.text = field} />
        <footer>
          <div className="input-group">
            <label>Urgency level: </label>
            <select defaultValue={urgency} ref={field => this.note.urgency = field}>
              <option value="">Urgency level</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
          </div>
          <div className="input-group">
            <label>Completed? </label>
            <input type="checkbox" defaultChecked={isCompleted} ref={field => this.note.isCompleted = field} />
          </div>
        </footer>
        {msgError !== '' && <div className="error-msg">{msgError}</div>}
        <div className="note-buttons">
          <button type="button" onClick={this.save}>
            <FontAwesomeIcon icon={faSdCard} size="2x" />
          </button>
          <button type="button" onClick={this.delete}>
            <FontAwesomeIcon icon={faTrashAlt} size="2x" />
          </button>
        </div>
      </div>
    )
  }

}

Note.propTypes = {
  id: PropTypes.string,
  text: PropTypes.string,
  created: PropTypes.string,
  updated: PropTypes.string,
  isCompleted: PropTypes.bool,
  urgency: PropTypes.number,
  saveNote: PropTypes.func,
  deleteNote: PropTypes.func,
}


export default Note
