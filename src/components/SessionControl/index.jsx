import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  createSession,
  alterSession,
  deleteSession,
  toggleLoading,
  getAllNotes,
  clearError,
} from '../../actions/todos/index'

class SessionControl extends Component {

  state = {
    msgError: '',
    currentRate: null,
  }

  createAlterSession = async () => {
    this.setState({ msgError: '' })
    let errorRate = this.errorRate && this.errorRate.value
    if (!errorRate || errorRate === '') {
      this.setState({ msgError: 'Please define a value to create/alter your session' })

      return
    }

    errorRate = parseInt(errorRate, 10)

    const {
      createSession: createSessionAction,
      alterSession: alterSessionAction,
      toggleLoading: toggleLoadingAction,
      getAllNotes: getAllNotesAction,
      clearError: clearErrorAction,
    } = this.props

    let { sessionId } = this.props

    if (errorRate < 0 || errorRate > 100) {
      this.setState({ msgError: 'Error rate must be between 0 and 100' })

      return
    }

    let { errorInfo } = this.props


    toggleLoadingAction(true)
    clearErrorAction()
    if (sessionId) await alterSessionAction(errorRate, sessionId)
    else await createSessionAction(errorRate, sessionId);
    ({ errorInfo } = this.props)
    if (errorInfo.error) {
      this.setState({ msgError: errorInfo.errorMessage })
      toggleLoadingAction(false)

      return
    }
    this.errorRate.value = ''
    const { errorRate: currentRate } = this.props;
    ({ sessionId } = this.props)
    clearErrorAction()
    await getAllNotesAction(sessionId);
    ({ errorInfo } = this.props)
    if (errorInfo.error) {
      this.setState({ msgError: errorInfo.errorMessage })
      toggleLoadingAction(false)

      return
    }
    this.setState({ currentRate }, () => toggleLoadingAction(false))
  }

  deleteSession = async () => {
    const {
      sessionId,
      deleteSession: deleteSessionAction,
      toggleLoading: toggleLoadingAction,
      clearError: clearErrorAction,
    } = this.props
    toggleLoadingAction(true)
    clearErrorAction()
    await deleteSessionAction(sessionId)

    const { errorRate: currentRate } = this.props
    this.setState({ currentRate }, () => toggleLoadingAction(false))
  }

  render() {
    const { className, sessionId } = this.props
    const { msgError, currentRate } = this.state

    return (
      <header className={`session-creator ${className || ''}`}>
        <div className="create-alter-session">
          <div className="form">
            <input type="number" tabIndex="0" ref={field => this.errorRate = field} placeholder="Error rate" />
          </div>
          <div className="session-buttons">
            <div>
              <div className="big-button" role="button" tabIndex="0" onClick={this.createAlterSession}>
                {sessionId ? 'Alter Session' : 'Create Session'}
              </div>
            </div>
            {sessionId && (
              <div>
                <div className="big-button" role="button" tabIndex="0" onClick={this.deleteSession}>
                  Delete Session
                </div>
              </div>
            )}
          </div>
          {msgError !== '' && <div className="error-msg">{msgError}</div>}
        </div>
        <div>
          <h4>{currentRate !== null && currentRate !== undefined ? `Error rate: ${currentRate}` : ''}</h4>
        </div>
      </header>
    )
  }

}

SessionControl.propTypes = {
  createSession: PropTypes.func.isRequired,
  alterSession: PropTypes.func.isRequired,
  deleteSession: PropTypes.func.isRequired,
  getAllNotes: PropTypes.func.isRequired,
  clearError: PropTypes.func.isRequired,
  toggleLoading: PropTypes.func,
  className: PropTypes.string,
  sessionId: PropTypes.string,
  errorRate: PropTypes.number,
}

function mapStateToProps(todos) {
  return {
    ...todos,
  }
}

const actionCreator = () => (
  {
    createSession,
    alterSession,
    deleteSession,
    toggleLoading,
    getAllNotes,
    clearError,
  }
)

export default connect(mapStateToProps, actionCreator())(SessionControl)
