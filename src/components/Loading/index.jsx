import React, { Component } from 'react'
import ReactLoading from 'react-loading'

class Loading extends Component {

  render() {

    return (
      <div className="loading">
        <div className="bg-loading" />
        <ReactLoading type="spin" className="loading-spin" />
      </div>
    )
  }

}

export default Loading
