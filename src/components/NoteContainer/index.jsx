import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import Note from '../Note'
import NoteFilter from '../NoteFilter'

class NoteContainer extends Component {

  state = {
  }

  updateFilter = (field, value) => {
    this.setState(prevState => ({ filters: { ...prevState.filters, [field]: value } }))
  }

  render() {

    const { notes, className, creatingNote, saveNote, deleteNote } = this.props
    const { filters } = this.state

    let filteredNotes = notes

    if (filters) {
      Object.keys(filters).map((f) => {
        filteredNotes = filteredNotes.filter((n) => {
          if (filters[f] === null) return true

          return n[f] === filters[f]
        })

        return null
      })
    }

    return (
      <Fragment>
        <NoteFilter updateFilter={this.updateFilter} />
        <div className={`note-container ${className || ''}`}>
          {creatingNote && (
            <Note saveNote={saveNote} deleteNote={deleteNote} />
          )}
          {filteredNotes && filteredNotes.map(note => (
            <Note {...note} key={note.id} saveNote={saveNote} deleteNote={deleteNote} />
          ))}
        </div>
      </Fragment>
    )
  }

}

NoteContainer.propTypes = {
  id: PropTypes.string,
  text: PropTypes.string,
  created: PropTypes.instanceOf(Date),
  updated: PropTypes.instanceOf(Date),
  isCompleted: PropTypes.bool,
  urgency: PropTypes.number,
  className: PropTypes.string,
  saveNote: PropTypes.func,
  deleteNote: PropTypes.func,
}


export default NoteContainer
