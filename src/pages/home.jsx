import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { faStickyNote } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import NoteContainer from '../components/NoteContainer'
import {
  createNote,
  getAllNotes,
  updateNote,
  deleteNote,
  toggleLoading,
} from '../actions/todos'

class Home extends Component {

  state = {
    msgError: '',
    creatingNote: false,
  }

  async componentDidMount() {
    const { sessionId, getAllNotes: getAllNotesAction, toggleLoading: toggleLoadingAction } = this.props
    let { errorInfo } = this.props
    if (sessionId) {
      toggleLoadingAction(true)
      await getAllNotesAction(sessionId);
      ({ errorInfo } = this.props)

      if (errorInfo.error) {
        this.setState({ msgError: errorInfo.errorMessage })
        toggleLoadingAction(false)
      }
    }
  }

  saveNote = async (note) => {
    const { updateNote: updateNoteAction, createNote: createNoteAction, sessionId, getAllNotes: getAllNotesAction } = this.props

    if (!note.id) {
      await createNoteAction(note, sessionId)
    } else {
      await updateNoteAction(note, sessionId)
    }

    this.setState({ creatingNote: false })
    await getAllNotesAction(sessionId)
  }

  deleteNoteHandler = async (id) => {
    if (!id) {
      this.setState({ creatingNote: false })

      return
    }

    const {
      deleteNote: deleteNoteAction,
      sessionId,
      getAllNotes: getAllNotesAction,
      toggleLoading: toggleLoadingAction,
    } = this.props
    let { errorInfo } = this.props

    toggleLoadingAction(true)

    await deleteNoteAction(id, sessionId);
    ({ errorInfo } = this.props)

    if (errorInfo.error) {
      this.setState({ msgError: errorInfo.errorMessage })
      toggleLoadingAction(false)

      return
    }
    await getAllNotesAction(sessionId);
    ({ errorInfo } = this.props)

    if (errorInfo.error) {
      this.setState({ msgError: errorInfo.errorMessage })
    }
    toggleLoadingAction(false)
  }

  newNote = () => {
    const { creatingNote } = this.state
    if (creatingNote) return
    this.setState({ msgError: '' })
    this.setState({ creatingNote: true })
  }

  render() {
    const { msgError, creatingNote } = this.state
    const { todos, sessionId } = this.props

    return (
      <section className="home">
        {sessionId
          ? (
            <Fragment>
              <header>
                <nav>
                  <button type="button" onClick={this.newNote}>
                    <FontAwesomeIcon icon={faStickyNote} size="3x" />
                    <p>New</p>
                  </button>
                </nav>
              </header>
              <NoteContainer
                notes={todos}
                saveNote={this.saveNote}
                creatingNote={creatingNote}
                deleteNote={this.deleteNoteHandler}
              />
              {msgError !== '' && <div className="error-msg">{msgError}</div>}
            </Fragment>
          )
          : (
            <h2>No open session</h2>
          )
        }
      </section>
    )
  }

}

function mapStateToProps(todos) {
  return {
    ...todos,
  }
}

const actionCreator = () => (
  {
    createNote,
    getAllNotes,
    updateNote,
    deleteNote,
    toggleLoading,
  }
)

Home.propTypes = {
  todos: PropTypes.array,
  sessionId: PropTypes.string,
  createNote: PropTypes.func,
  updateNote: PropTypes.func,
  deleteNote: PropTypes.func,
  toggleLoading: PropTypes.func,
}

export default connect(
  mapStateToProps,
  actionCreator(),
)(Home)
