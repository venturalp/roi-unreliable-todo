import React, { Component, Fragment } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Home from './pages/home'
import SessionControl from './components/SessionControl'
import Loading from './components/Loading'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

class App extends Component {

  render() {
    const { isLoading } = this.props

    return (
      <BrowserRouter>
        <Fragment>
          {isLoading && <Loading />}
          <SessionControl />
          <Switch>
            <Route path="/" exact render={() => <Home />} />
            <Route render={() => "ERROR 404"} />
          </Switch>
        </Fragment>
      </BrowserRouter>
    )
  }

}

App.propTypes = {
  isLoading: PropTypes.bool,
}

function mapStateToProps(todos) {
  return {
    ...todos,
  }
}

export default connect(mapStateToProps)(App)
