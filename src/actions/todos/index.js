import { URL_API } from '../../utils/constants'
import * as Actions from './types'

const validateResponse = (response, errorMessage, dispatch) => {
  if (response.status.toString().substr(0, 1) !== '2') {
    dispatch({
      type: Actions.ERROR,
      error: {
        error: true,
        errorMessage,
      },
    })

    return false
  }

  return true
}

export const createSession = errorRate => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/session`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ errorRate }),
    })

    if (!validateResponse(response, `Erro trying to create a session (${response.status})`, dispatch)) return

    const result = await response.json()
    dispatch({
      type: Actions.CREATE_SESSION,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to create a session',
      },
    })
  }
}

export const alterSession = (errorRate, sessionId) => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/session`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        sessionId,
      },
      body: JSON.stringify({ errorRate }),
    })

    if (!validateResponse(response, `Erro trying to alter a session (${response.status})`, dispatch)) return
    const result = await response.json()

    dispatch({
      type: Actions.ALTER_SESSION,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to alter a session',
      },
    })
  }
}

export const toggleLoading = value => dispatch => (
  dispatch({
    type: Actions.TOGGLE_LOADING,
    value,
  })
)

export const deleteSession = sessionId => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/session`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        sessionId,
      },
    })

    if (!validateResponse(response, `Erro trying to delete a session (${response.status})`, dispatch)) return
    const result = await response.json()

    dispatch({
      type: Actions.DELETE_SESSION,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to delete a session',
      },
    })
  }
}

export const createNote = (note, sessionId) => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/todos`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        sessionId,
      },
      body: JSON.stringify(note),
    })

    if (!validateResponse(response, `Erro trying to create a todo (${response.status})`, dispatch)) return
    const result = await response.json()

    dispatch({
      type: Actions.CREATE_TODO,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to create a todo',
      },
    })
  }
}

export const updateNote = (note, sessionId) => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/todos/${note.id}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        sessionId,
      },
    })

    if (!validateResponse(response, `Erro trying to update a todo (${response.status})`, dispatch)) return
    const result = await response.json()

    dispatch({
      type: Actions.ALTER_TODO,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to update a todo',
      },
    })
  }
}

export const deleteNote = (id, sessionId) => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/todos/${id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        sessionId,
      },
    })

    if (!validateResponse(response, `Erro trying to delete a todo (${response.status})`, dispatch)) return
    const result = await response.json()

    dispatch({
      type: Actions.DELETE_TODO,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to delete a todo',
      },
    })
  }
}

export const getAllNotes = sessionId => async (dispatch) => {
  try {
    const response = await fetch(`${URL_API}/todos`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        sessionId,
      },
    })

    if (!validateResponse(response, `Erro trying to get all todos (${response.status})`, dispatch)) return
    const result = await response.json()

    dispatch({
      type: Actions.GET_ALL_TODOS,
      payload: result,
    })
  } catch (err) {
    dispatch({
      type: Actions.ERROR,
      error: {
        logErr: { ...err },
        error: true,
        errorMessage: 'Erro trying to get all todos',
      },
    })
  }
}

export const clearError = () => dispatch => dispatch({ type: Actions.CLEAR_ERROR })
