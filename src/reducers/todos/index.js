import {
  CREATE_SESSION,
  CREATE_TODO,
  ALTER_SESSION,
  DELETE_SESSION,
  GET_ALL_TODOS,
  ERROR,
  TOGGLE_LOADING,
  CLEAR_ERROR,
} from '../../actions/todos/types'

const INITIAL_STATE = {
  sessionId: '',
  todos: [],
  errorRate: null,
  isLoading: false,
  errorInfo: { error: false },
}

const formatTodos = todos => Object.keys(todos).map(t => todos[t])

function todo(state = INITIAL_STATE, action) {
  switch (action.type) {
    case CREATE_SESSION:
      return {
        ...state,
        errorRate: action.payload.errorRate,
        sessionId: action.payload.sessionId,
      }
    case ALTER_SESSION:
      return {
        ...state,
        errorRate: action.payload.errorRate,
      }
    case DELETE_SESSION:
      return {
        ...INITIAL_STATE,
      }
    case CREATE_TODO:
      return {
        ...state,
        todos: [...state.todos, { ...action.todo }],
      }
    case ERROR:
      return {
        ...state,
        errorInfo: { ...action.error },
      }
    case TOGGLE_LOADING:
      return {
        ...state,
        isLoading: action.value === undefined ? !state.isLoading : action.value,
      }
    case GET_ALL_TODOS:
      return {
        ...state,
        todos: formatTodos(action.payload.todos),
      }
    case CLEAR_ERROR:
      return {
        ...state,
        errorInfo: INITIAL_STATE.errorInfo,
      }
    default:
      return state
  }

}

export default todo
